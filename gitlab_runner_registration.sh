#!/bin/bash

# First, we need to have the configuration stored, the typical way is to have it in a docker volume. We'll
# do it this way, the name of our volume will be: gitlab-runner-config

# In case the volume already exists, then we have to delete to create a clean one (apparently, if we use
# an existing one then the gitlab-runner container won't register correctly our machine)
docker volume rm gitlab-runner-config

# The following command starts a container which only has one objective: execute the program that registers
# our machine, for this reason we use the --rm option (we won't need that container anymore). Once the 
# registration process is finished the configuration file will be stored in our volume
docker run --rm -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image docker:19.03.12 \
  --url "https://gitlab.com/" \
  --registration-token "sLxiCsLZUoeXEXssbr6c" \
  --description "Hulios_runner" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected" \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-privileged
