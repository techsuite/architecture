#!/bin/bash
# In order to get this to work we need to run the configuration 
# and store in the gitlab-runner-config volume
docker run \
	--rm \
	--detach \
	-v gitlab-runner-config:/etc/gitlab-runner \
	-v /var/run/docker.sock:/var/run/docker.sock \
        gitlab/gitlab-runner	
